#!/bin/sh

ID=${1}
FEATUREVER=1.8.0

if [ "x${ID}" = "x" ] ; then
    echo "$0 <ID>";
    exit 1;
fi

if [ "x${TMPDIR}" = "x" ] ; then
    TMPDIR=/tmp
fi

downloaddir=${TMPDIR}/download.$$
mkdir ${downloaddir}
pushd ${downloaddir}
echo "Downloading build ${ID} in ${downloaddir}";
brew download-build ${ID}

versionregexp="[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*\.b[0-9]*-[0-9]*"
basename=$(ls|grep java-${FEATUREVER}-openjdk-portable-unstripped-${versionregexp}.el7openjdkportable.x86_64.rpm)
version=$(echo ${basename}|sed -r "s|^.*-(${versionregexp})\.el7.*$|\1|")

echo "Downloaded version ${version}"

# Remove stripped release builds for portable and JREs
rm -vf java-${FEATUREVER}-openjdk-portable-${FEATUREVER}*
rm -vf java-${FEATUREVER}-openjdk-portable-devel-${FEATUREVER}*
rm -vf java-${FEATUREVER}-openjdk-portable-slowdebug-${FEATUREVER}*
rm -vf java-${FEATUREVER}-openjdk-portable-fastdebug-${FEATUREVER}*

mkdir unpacked
for file in *.rpm; do
    cat ${file} | rpm2archive - | tar -C unpacked -xzv
done

mkdir ${HOME}/${version}
mv unpacked/usr/lib/jvm/* ${HOME}/${version}

pushd ${HOME}/${version}
for file in *.sha256sum; do
    if ! sha256sum --check ${file} ; then
	echo "${file} failed checksum.";
	exit 2;
    fi
done
popd

rm -rf unpacked
echo rm -vf *.rpm

popd

